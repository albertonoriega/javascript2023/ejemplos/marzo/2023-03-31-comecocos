const posiciones = [
    0, // Coordenada X
    0, // Coordenada Y
];

//Constante con las dimensiones del lienzo
const dimensiones = [
    500, //Ancho
    500, // Alto
];

// Constante que apunta a la pieza
const pieza = document.querySelector('#pieza');

// Situamos la pieza en la posicion 0 en X
pieza.style.left = posiciones[0] + "px";
// Situamos la pieza en la posicion 0 en Y
pieza.style.top = posiciones[1] + "px";

// variable para controlar el desplazamiento
let paso = 10;

let direccion = "derecha"; // Por defecto se va a mover a la derecha

// Variable que controla el timing setInterval
let temporizador = null;

//Variable de puntuacion
let puntacion = 0;

function gameOver() {
    // Paramos el temporizador de movimiento
    clearInterval(temporizador);
    // Preguntamos si quiere volver a jugar
    if (confirm("¿Desea volver a jugar?")) {
        // Recargar la página => F5
        location.reload();
    }

}

// Ponemos un escuchador en el body para que controle la pulsación de los cursores
// keydown => cuando aprietas una tecla
document.querySelector('body').addEventListener('keydown', function (evento) {
    //variable que almacena la tecla que se ha pulsado
    let teclaPulsada = null;
    teclaPulsada = evento.key;

    // Dependiendo de que tecla se pulse, cambiamos la variable direccion y la imagen de fondo 
    switch (teclaPulsada) {
        case 'ArrowDown':
            direccion = "abajo";
            pieza.style.backgroundImage = "url('comecocosAbajo.png')";
            break;
        case 'ArrowUp':
            direccion = "arriba"
            pieza.style.backgroundImage = "url('comecocosArriba.png')";
            break;
        case 'ArrowRight':
            direccion = "derecha";
            pieza.style.backgroundImage = "url('comecocosD.png')";
            break;
        case 'ArrowLeft':
            direccion = "izquierda";
            pieza.style.backgroundImage = "url('comecocosIzquierda.png')";
            break;
    }
});

//control de tiempos, control del evento setInterval

temporizador = window.setInterval(() => {

    //constante que apunta al marcador
    const marcador = document.querySelector('.marcador span');

    /**
     * Función que controla la colisiones => si la pieza se sale del lienzo se detiene el juego
     */
    function controlColisiones() {

        //(-19 es para que no se salga del lienzo por la derecha y por abajo ya que la pieza tiene un ancho de 20px (radio de 10px))
        //Coordenada X 
        if (posiciones[0] < 0 || posiciones[0] >= dimensiones[0] - 19) {
            gameOver(); // Paramos el juego llamando a la funcion gameOver
            // Coordenada Y
        } else if (posiciones[1] < 0 || posiciones[1] >= dimensiones[1] - 19) {
            gameOver();
        }
    }

    switch (direccion) {
        case "izquierda":
            posiciones[0] -= paso;
            break;
        case "derecha":
            posiciones[0] += paso;
            break;
        case "abajo":
            posiciones[1] += paso;
            break;
        case "arriba":
            posiciones[1] -= paso;
            break;
    }

    //Compruebo si estoy fuera del lienzo para detener el juego
    controlColisiones();

    // Si no se ha chocado se incrementa la puntación en 1
    puntacion++;
    // Vamos actualizando la puntación del marcador
    marcador.innerHTML = puntacion;

    // Situamos la pieza en la coordenada X actual
    pieza.style.left = posiciones[0] + "px";
    // Situamos la pieza en la coordenada Y actual
    pieza.style.top = posiciones[1] + "px";
}, 100); // 100 son 100 milisegundos

//Cada 5 segundos hacemos que vaya más rápido
window.setInterval(function () {
    paso++;
}, 5000);